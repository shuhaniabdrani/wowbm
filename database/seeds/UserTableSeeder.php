<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@wowbm.com',
            'password' =>bcrypt('aishah'),
            'role' => 1
        ]);

        User::create([
            'name' => 'hani',
            'email' => 'hani@user.com',
            'password' =>bcrypt('hanini'),
            'role' => 2
        ]);
    }
}
