<?php

use Illuminate\Database\Seeder;
use App\Models\ExerciseType;

class ExerciseTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
    		array('name' => 'Reading'),
    		array('name' => 'Writing'),
            array('name' => 'Listening'),
            array('name' => 'Speaking'),
    	);

        ExerciseType::insert($data);
    }
}
