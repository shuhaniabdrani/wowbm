<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
    		array('name' => 'Admin'),
    		array('name' => 'Student'),
            array('name' => 'Teacher'),
    	);

        Role::insert($data);
    }
}
