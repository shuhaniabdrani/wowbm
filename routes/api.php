<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', 'Auth\UserController@current');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');

    // ***API used in admin front end****
    // Topic
    Route::post('/topic/paginate','TopicController@getTopicPaginate'); //paginate list topic
    Route::post('/topic/store', 'TopicController@store'); // create new topic 
    Route::post('/topic/edit','TopicController@edit'); //edit a topic
    Route::post('{topic_id}/document/store', 'TopicController@documentStore');  // save document, belum implement
    Route::delete('/topic/{topic_id}/delete', 'TopicController@delete');
    Route::post('/topic/{topic_id}/uploadFile', 'TopicController@uploadNoteFile');
    
    // Exercise and Question
    Route::post('/exercise/paginate','ExerciseController@getExercisePaginate'); //paginate list exercise
    Route::post('/exercise/create','ExerciseController@create'); //create new exercise
    Route::post('/exercise/edit','ExerciseController@edit'); //edit exercise
    Route::delete('/exercise/{exercise_id}/delete', 'ExerciseController@delete');

    Route::post('/exercise/{exercise_id}/question/paginate','QuestionController@getQuestionPaginate'); //paginate to be used in admin front end
    Route::post('/exercise/{exercise_id}/question/create','QuestionController@create'); //add new question on exercise
    Route::post('/question/{question_id}/edit','QuestionController@edit'); //edit a question

    Route::post('/question_option/{option_id}/uploadImage', 'QuestionController@uploadOptionImage');

    //vocabulary
    Route::post('/vocabulary/paginate','VocabularyController@getVocabPaginate'); //paginate list vocab
    Route::post('/vocabulary/create', 'VocabularyController@create'); // create new vocab 
    Route::post('/vocabulary/edit', 'VocabularyController@edit'); // create new vocab 
    Route::delete('/vocabulary/{vocab_id}/delete', 'VocabularyController@delete'); // delete

    //analysis
    Route::post('/exercise/resultlist/paginate','ExerciseController@getExerciseResultPaginate'); //paginate to result list


    // *****API*****
    Route::get('/topiclist', 'TopicController@getAllTopic'); // get all topic data
    Route::get('/topic/{topic_id}', 'TopicController@getTopicData');
    Route::get('/topicanalysis/all','TopicController@getAllTopicAnalysis');
    Route::get('/exerciselist', 'ExerciseController@getAllExercise'); //get all exercise data
    Route::get('/exercise/{exercise_id}', 'ExerciseController@getExerciseData'); //get exercise data by exercise id
    Route::get('/exercise/{exercise_id}/questions', 'ExerciseController@getExerciseQuestions'); // get all exercise questions by exercise_id
    Route::get('/question/{question_id}','QuestionController@getQuestion');//get any question by id
    Route::post('/exercise/{exercise_id}/saveAnswer', 'ExerciseController@saveAnswer');
    Route::get('/attempt/{attempt_id}/result', 'ExerciseController@getAttemptResult');
    Route::get('/vocablist', 'VocabularyController@getAllVocab'); // get all topic data


});

Route::group(['middleware' => 'guest:api'], function () {
    // Route::post('login', 'Auth\LoginController@login');
    // Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'AuthController@sendPasswordResetLink');
    Route::post('password/reset', 'AuthController@callResetPassword');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');

    Route::post('vocabulary/add', 'VocabularyController@create');
    Route::get('vocabulary/all', 'VocabularyController@getAll');

    

});

Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});

//ni fatin helo
