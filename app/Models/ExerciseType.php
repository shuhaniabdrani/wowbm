<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExerciseType extends Model
{
    protected $casts = [
        'name' => 'string'
    ];

    protected $table = "exercise_types";

    protected $fillable = [
        'name',
    ];

}
