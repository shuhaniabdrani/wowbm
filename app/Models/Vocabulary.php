<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Vocabulary extends Model
{
    protected $casts = [
        'word' => 'string',
        'meaning' => 'string',

      ];
  
      protected $table = "vocabularies";
  
      protected $fillable = [
        'word','meaning',
      ];
  

}
