<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SampleAnswer extends Model
{
    protected $fillable = [
        'question_id',
        'text',
    ];
}
