<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $casts = [
        'topic_name' => 'string',
        'description' => 'string',
        'file' => 'string'
    ];
  
    protected $table = "topics";

    protected $fillable = [
        'topic_name', 'description','file'
    ];
}
