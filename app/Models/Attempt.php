<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Attempt extends Model
{
    protected $fillable = [
        'user_id', 'exercise_id', 'result', 'total_questions'
    ];

    public function answers(){
        return $this->hasMany(Answer::class, 'attempt_id');
    }

    
}
