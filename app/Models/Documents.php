<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $casts = [
        'topicname' => 'string',
        'description' => 'string',
        'file'=>'string',

      ];
  
      protected $table = "documents";
  
      protected $fillable = [
        'topicname','description','file',
      ];
  
}
