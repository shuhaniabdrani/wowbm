<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $casts = [
      'name' => 'string'
    ];

    protected $table = "roles";

    protected $fillable = [
      'name',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}