<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question_text', 
        'answer_explanation', 
        'topic_id',
        'question_type'
    ];

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function options()
    {
        return $this->hasMany(QuestionsOption::class, 'question_id');
    }

    public function exercises(){
        return $this->belongsToMany('App\Models\Exercise');
    }
}
