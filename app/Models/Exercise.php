<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $fillable = [
        'exercise_name', 'exercise_type_id' 
    ];

    // public function questions(){
    //     return $this->belongsToMany('App\Models\Question');
    // }

    public function questions()
    {
        return $this->hasMany(Question::class, 'exercise_id');
    }

    public function attempts()
    {
        return $this->hasMany(Question::class, 'exercise_id');
    }
}
