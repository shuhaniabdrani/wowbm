<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'exercise_id', 'question_id' , 'correct'
    ];

    protected $table = "answers";

    public function question(){
        return $this->hasOne(Question::class, 'question_id');
    }
}
