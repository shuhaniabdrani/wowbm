<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Topic;
use App\Http\Services\TopicService;
use App\Models\Answer;
use App\Models\Attempt;
use App\Models\Exercise;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TopicController extends Controller
{
    public function __construct(
        TopicService $topic
    )
    {
        $this->topic = $topic;
    }

    public function getTopicPaginate(Request $request)
    {
        $response = $this->topic->getTopicPaginate($request);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'topic_name' => 'required'
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        $response = $this->topic->create($request);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'topic_name' => 'required',
            'description' => 'required'
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors(),
                'message' => 'Required fields missing'
            ], 422);
        }

        $data = new Topic();
        $data->topic_name = $request->topic_name;
        if(isset($data->description))
            $data->description = $request->description;
        
        $data->save();

        return response()->json(['status' => 'success', 'result' => $data], 200);

    }

    public function documentStore(Request $request, $topic_id)
    {
        $topic = Topic::find($topic_id);

        $files = $request->file('file');
        $filename = $files->getClientOriginalName();
        // dd($filename);

        $topic->update(['file' => $filename]);
        

        return response()->json(['status' => 'success', 'result' => $topic], 200);
        
    }

    public function edit(Request $request)
    {
        $v = Validator::make($request->all(), [
            'topic_name' => 'required'
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors(),
                'message' => 'Required fields missing'
            ], 422);
        }

        $response = $this->topic->edit($request);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function delete($topic_id)
    {
        $topic = Topic::find($topic_id);
        $topic->delete();

        return response()->json(['status' => 'success'], 200);
    }
    /**
     *
     * @OA\Get(
     *
     *  path="/api/topiclist",
     *  operationId="topiclist",
     *  tags={"topiclist"},
     *  summary="Get all topic data",
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Data Retrieved",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function getAllTopic()
    {
        $topics = Topic::get();

        foreach($topics as $topic){
            if($topic->file)
                $topic->file_url = env('APP_URL') . '/lib/pdfjs-2.9.359-dist/web' . $topic->file;
        }

        return response()->json(['status' => 'success', 'result' => $topics], 200);
    }

    public function uploadNoteFile(Request $request, $topic_id)
    {
        $file = $request->file('file');
        
        if(isset($file))
        {
            // $validation type
            if($file->extension() == 'pdf'){
                $upload_path = public_path('/lib/pdfjs-2.9.359-dist/web/upload/'.$topic_id.'/');
                // $original_name = $file->getClientOriginalName();
                $generated_new_name = 'file.' . $file->getClientOriginalExtension();
                $file->move($upload_path, $generated_new_name);

                $topic = Topic::find($topic_id);

                $topic->update([
                    'file' => '/upload/'.$topic_id.'/'.$generated_new_name
                ]);

                $response = [
                    'status' => '200',
                    'message' => 'success',
                    'result' => '/upload/'.$topic_id.'/'.$generated_new_name,
                    'topic' => $topic
                ];
            } 
            else{
                return response()->json([
                    'message' => 'Format not supported. PDF File Only'
                ], 422);
                // $response = [
                //     'status' => 400,
                //     'message' => 'PDF File Only',
                // ];
            }
            return response()->json($response);
        }
        $response = [
            'status' => '200',
            'message' => 'No file Uploaded',
            'result' => $file
        ];
        return response()->json($response);
    }
    /**
     *
     * @OA\Get(
     *
     *  path="/api/topic/{topic_id}",
     *  operationId="topic",
     *  tags={"topic"},
     *  summary="Get topic data by id",
     * @OA\Parameter(
     *      name="topic_id",
     *      description="topic id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer",
     *          description="",
     *          default="1"
     *      )
     *  ),
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Data Retrieved",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function getTopicData($topic_id)
    {
        $topic = Topic::find($topic_id);

        
        if($topic->file)
            $topic->file_url = env('APP_URL') . '/lib/pdfjs-2.9.359-dist/web/' . $topic->file;
        

        return response()->json(['status' => 'success', 'result' => $topic], 200);
    }
    /**
     *
     * @OA\Get(
     *
     *  path="/api/topicanalysis/all",
     *  operationId="topicanalysis",
     *  tags={"topicanalysis"},
     *  summary="Get all topic analysis data",
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Data Retrieved",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function getAllTopicAnalysis()
    {
        $topics = Topic::select('id', 'topic_name')->get();
        $exercise = Exercise::select('id')->get();

        $attempt_id = array();
        // get latest attempt for each exercise for a user
        foreach($exercise as $exe){
            $exe->attempt = Attempt::select('id')->where('user_id', Auth::user()->id)->where('exercise_id', $exe->id)->orderBy('updated_at', 'desc')->first();
            array_push($attempt_id, $exe->attempt->id);
        }
        
        foreach($topics as $topic){ 
            // count all answer based on attempt_ids
            $answers = Answer::wherein('attempt_id', $attempt_id)->where('topic_id', $topic->id)->get();
            $topic->total_answer = count($answers);
            
            // count the correct answer
            $correct_answer = Answer::wherein('attempt_id', $attempt_id)->where('topic_id', $topic->id)->where('correct', 1)->get();
            $topic->total_correct = count($correct_answer);
        }

        return response()->json(['status' => 'success', 'result' => $topics], 200);
    }
}
