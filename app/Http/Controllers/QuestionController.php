<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\QuestionService;
use App\Http\Services\QuestionsOptionService;
use App\Models\Question;
use App\Models\QuestionsOption;
use App\Models\SampleAnswer;
use Illuminate\Support\Facades\Storage;

class QuestionController extends Controller
{
    public function __construct(
        QuestionService $question,
        QuestionsOptionService $question_option
    )
    {
        $this->question = $question;
        $this->question_option = $question_option;
    }

    public function getQuestionPaginate(Request $request, $exercise_id)
    {
        $response = $this->question->getQuestionPaginate($request, $exercise_id);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function create(Request $request, $exercise_id)
    {
        
        $question = $this->question->create($request, $exercise_id);

        $option = QuestionsOption::where('question_id', $question->id)->get();

        return response()->json(['status' => 'success', 'question' => $question, 'question_option' => $option], 200);

    }

    public function edit(Request $request, $question_id)
    {
        $question = $this->question->edit($request, $question_id);

        $option = QuestionsOption::where('question_id', $question->id)->get();

        return response()->json(['status' => 'success', 'question' => $question, 'question_option' => $option], 200);
    }

    public function getQuestion($question_id)
    {

        $question = Question::find($question_id);
        // $option = Question::find($id)->options();
        if($question->question_type == 1){
            $option = QuestionsOption::where('question_id', $question_id)->get();
            foreach($option as $opt){
                if($opt->image)
                    $opt->image_url = env('APP_URL') . $opt->image;
            }
        }
        else if($question->question_type == 2){
            $option = SampleAnswer::where('question_id', $question_id)->get();
        }

        return response()->json(['status' => 'success', 'question' => $question, 'question_option' => $option], 200);
    }

    public function getQuestionList()
    {
        $list = Question::select('id', 'question_text')->get();

        return response()->json(['status' => 'success', 'result' => $list], 200);
    }

    public function uploadOptionImage(Request $request, $option_id)
    {
        $file = $request->file('image');
        
        if(isset($file))
        {
            // $validation type
            if($file->extension() == 'jpg' || $file->extension() == 'jpeg' ||  $file->extension() == 'png'){
                $upload_path = public_path('upload/option/'.$option_id.'/');
                $generated_new_name = 'image.' . $file->getClientOriginalExtension();
                $file->move($upload_path, $generated_new_name);

                $option = QuestionsOption::find($option_id);

                $option->update([
                    'image' => '/upload/option/'.$option_id.'/'.$generated_new_name
                ]);

                $response = [
                    'status' => '200',
                    'message' => 'success',
                    'result' => '/upload/option/'.$option_id.'/'.$generated_new_name
                ];
            } 
            else{
                $response = [
                    'status' => 400,
                    'message' => 'Image File Only',
                ];
            }
            return response()->json($response);
        }
        $response = [
            'status' => '200',
            'message' => 'No file Uploaded',
            'result' => $file
        ];
        return response()->json($response);
    }
}
