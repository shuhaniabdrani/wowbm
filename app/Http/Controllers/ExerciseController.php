<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\ExerciseService;
use App\Http\Services\QuestionService;
use App\Models\Exercise;
use App\Models\QuestionsOption;
use App\Models\Question;
use Illuminate\Support\Facades\Validator;


class ExerciseController extends Controller
{
    public function __construct(
        ExerciseService $exercise,
        QuestionService $question
    )
    {
        $this->exercise = $exercise;
        $this->question = $question;
    }

    public function getExercisePaginate(Request $request)
    {
        $response = $this->exercise->getExercisePaginate($request);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'exercise_name' => 'required',
            'exercise_type_id' => 'required',
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors(),
                'message' => 'Required fields missing'
            ], 422);
        }

        $response = $this->exercise->create($request);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function edit(Request $request)
    {
        $response = $this->exercise->edit($request);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function delete($exercise_id)
    {
        $exercise = Exercise::find($exercise_id);
        $exercise->delete();

        return response()->json(['status' => 'success'], 200);
    }
     /**
     *
     * @OA\Get(
     *
     *  path="/api/exerciselist",
     *  operationId="exerciselist",
     *  tags={"exerciselist"},
     *  summary="Get all exercise data",
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Data Retrieved",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function getAllExercise() {

        $response = $this->exercise->getAllExercise();

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }
    /**
     *
     * @OA\Get(
     *
     *  path="/api/exercise/{exercise_id}",
     *  operationId="exercise",
     *  tags={"exercise"},
     *  summary="Get exercise data by id",
     * @OA\Parameter(
     *      name="exercise_id",
     *      description="exercise id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer",
     *          description="",
     *          default="1"
     *      )
     *  ),
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Data Retrieved",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function getExerciseData($exercise_id)
    {
        $response = $this->exercise->getExerciseData($exercise_id);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }
    /**
     *
     * @OA\Get(
     *
     *  path="/api/exercise/{exercise_id}/questions",
     *  operationId="questions",
     *  tags={"exercise's questions"},
     *  summary="Get exercise's questions data by exercise id",
     * @OA\Parameter(
     *      name="exercise_id",
     *      description="exercise id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer",
     *          description="",
     *          default="1"
     *      )
     *  ),
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Data Retrieved",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function getExerciseQuestions($exercise_id)
    {
        $response = $this->question->getExerciseQuestions($exercise_id);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }
   
    public function saveAnswer(Request $request, $exercise_id)
    {

        $response = $this->exercise->saveAnswer($request, $exercise_id);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function getAttemptResult($attempt_id)
    {
        $response = $this->exercise->getAttemptResult($attempt_id);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function getExerciseResultPaginate(Request $request)
    {
        $response = $this->exercise->getExerciseResultPaginate($request);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }
}
