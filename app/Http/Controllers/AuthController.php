<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Http\Services\ThirdPartyLoginService;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="WOWBM application API Documentation",
 *      description="All API for WOWBM application. For any usage of locked API, please login and retrieve the access token and input them in authorization form at top left of this page. The access token will automatically recognize the user.<br><br>To gain access for locked API, please login and generate access token and assign the access token in Authorization form [Bearer <access_token>].",
 * )
 */
/**
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     securityScheme="api_key",
 *     name="Authorization"
 * )
 */
class AuthController extends Controller
{
    
    use SendsPasswordResetEmails, ResetsPasswords {
    SendsPasswordResetEmails::broker insteadof ResetsPasswords;
    ResetsPasswords::credentials insteadof SendsPasswordResetEmails;
    }
        
    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3|confirmed',
            'role' => 'required',
            'school' => 'required'
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->school = $request->school;
        $user->password = bcrypt($request->password);
        $user->save();

        Mail::to($user->email)->queue(new WelcomeMail($request->all()));

        return response()->json(['status' => 'success', 'user' => $user], 200);
    }
    /**
     *
     * @OA\Post(
     *
     *  path="/api/auth/login",
     *  operationId="login",
     *  tags={"Login"},
     *  summary="Login using email and password",
     *  @OA\Parameter(
     *      name="email",
     *      description="User email: admin@wowbm,.com - admin, hani@user.com - user",
     *      required=true,
     *      in="query",
     *      @OA\Schema(
     *          type="string",
     *          description="",
     *          default="admin@wowbm.com"
     *      )
     *  ),
     *  @OA\Parameter(
     *      name="password",
     *      description="Password",
     *      required=true,
     *      in="query",
     *      @OA\Schema(
     *          type="string",
     *          default="aishah"
     *      )
     *  ),
     *  @OA\Response(
     *      response=200,
     *      description="Logged in",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
        @OA\Response(
            response=401, 
            description="Invalid name/email.<br /> 
            Password must be at least 8 characters.<br />
            Wrong password. 
        "),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * )
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if ($token = $this->guard()->attempt($credentials)) {
            return response()->json([
                'status'    => 200,
                'message'   => 'Login Successfully',
                'result'    => [
                    'token' => $token,
                    'user'  => auth()->user()
                ]
            ], 200)->header('Authorization', $token);
        }
        return response()->json(['error' => 'login_error'], 401);
    }
    /**
     *
     * @OA\Get(
     *
     *  path="/api/auth/logout",
     *  operationId="logout",
     *  tags={"Logout"},
     *  summary="Logout to revoke token",
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Successfully logged out",
     *      @OA\JsonContent(
     *      )
     *  ),
        @OA\Response(
            response=300, 
            description="Wrong password. <br />
            Invalid name/email.
        "),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
    // public function refresh()
    // {
    //     if ($token = $this->guard()->refresh()) {
    //         return response()
    //             ->json(['status' => 'successs'], 200)
    //             ->header('Authorization', $token);
    //     }
    //     return response()->json(['error' => 'refresh_token_error'], 401);
    // }
    public function refresh(Request $request)
    {
        try {
            if($request->header('Authorization') != ""){
                if ($token = $this->guard()->refresh()) {
                    $result['message'] = 'Success';
                    return response()->json($result, Response::HTTP_OK)->header('Authorization', $token);
                }
            }
        }catch (TokenBlacklistedException $e) {
            return response()->json(['error' => 'token_expired'], Response::HTTP_UNAUTHORIZED);
        }catch (TokenExpiredException $e) {
            return response()->json(['error' => 'token_expired'], Response::HTTP_UNAUTHORIZED);
        }catch (TokenInvalidException $e) {
            return response()->json(['error' => 'token_invalid'], Response::HTTP_UNAUTHORIZED);
        }catch (JWTException $e) {
            return response()->json(['error' => 'token_absent'], Response::HTTP_UNAUTHORIZED);
        }
        return response()->json(['error' => 'token_absent'], Response::HTTP_UNAUTHORIZED);
    }
    private function guard()
    {
        return Auth::guard();
    }
    /**
     * Send password reset link. 
     */
    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }
    /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'message' => 'Password reset email sent.',
            'data' => $response
        ]);
    }
    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Email could not be sent to this email address.']);
    }
    /**
     * Handle reset password 
     */
    public function callResetPassword(Request $request)
    {
        return $this->reset($request);
    }
    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
        event(new PasswordReset($user));
    }
    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Password reset successfully.']);
    }
    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Failed, Invalid Token.']);
    }

}