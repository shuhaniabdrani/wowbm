<?php

namespace App\Http\Controllers;

use App\Models\Documents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    public function create(){
        //admin punya page to create/ upload
        return view();
    }
    
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'topicname' => 'required',
            'description' => 'required',
            'file'=>'required',
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }


        $data = new Documents;
        $data->topicname = $request->topicname;
        $data->description = $request->description;
        
        if($request->file('file')){
            $file=$request->file('file');
            $filename=time().'.'.$file->getClientOriginalExtension();
        
            $data->$file=$filename;
        }
        $data->save();

        return response()->json(['status' => 'success', 'documents' => $data], 200);

    }

    public function show()
    {

    }
}
