<?php

namespace App\Http\Controllers;

use App\Models\Vocabulary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VocabularyController extends Controller
{
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'word' => 'required',
            'meaning' => 'required'
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        // apa2 je = new 'Model name';
        $vocab = new Vocabulary;
        $vocab->word = $request->word;
        $vocab->meaning = $request->meaning;
        $vocab->save();

        return response()->json(['status' => 'success', 'vocabulary' => $vocab], 200);
    }

    public function edit(Request $request)
    {
        $v = Validator::make($request->all(), [
            'word' => 'required',
            'meaning' => 'required'
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        $vocab = Vocabulary::find($request->id);

        $vocab->update([
            'word' => $request->word,
            'meaning' => $request->meaning
        ]);

        return response()->json(['status' => 'success', 'result' => $vocab], 200);
    }
    /**
     *
     * @OA\Get(
     *
     *  path="/api/vocablist",
     *  operationId="vocablist",
     *  tags={"vocablist"},
     *  summary="Get all topic data",
     *  security={
     *         {
     *             "api_key": {}
     *         }
     *  },
     *  @OA\Response(
     *      response=200,
     *      description="Data Retrieved",
     *      @OA\JsonContent(
     *      )
     *  ),
     *  @OA\Response(response=400, description="Bad request"),
     *  @OA\Response(response=404, description="Resource Not Found"),
     *  @OA\Response(response=500, description="Internal Server Error"),
     * ),
     */
    public function getAll()
    {
        $vocab=Vocabulary::all();
        return response()->json(['status' => 'success', 'vocabulary' => $vocab], 200);

    }

    public function getVocabPaginate(Request $request)
    {
        //not yet efficient
        $query = Vocabulary::whereNotNull('word');

        if (isset($request->search)) {
            $data = $request->search;
            $query = $query->where(function($q) use ($data){
                $q->whereRaw('LOWER(word) like ?', ['%' . strtolower($data) . '%'])
                ->orWhereRaw('LOWER(meaning) like ?', ['%' . strtolower($data) . '%']);
            });
        }

        $response = $query->paginate($request->itemsPerPage);

        return response()->json(['status' => 'success', 'result' => $response], 200);
    }

    public function delete($vocab_id)
    {
        $vocab = Vocabulary::find($vocab_id);
        $vocab->delete();

        return response()->json(['status' => 'success'], 200);
    }

    public function getAllVocab()
    {
        $vocab = Vocabulary::get();

        return response()->json(['status' => 'success', 'result' => $vocab], 200);
    }


}
