<?php

namespace App\Http\Services;
use App\Models\Question;
use App\Models\QuestionsOption;
use App\Models\SampleAnswer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class QuestionService{

    public function getQuestionPaginate($request, $exercise_id)
    {
        // nak buat topic::all() cannot, so alterntive hm
        $query = Question::where('exercise_id', $exercise_id);

        if (isset($request->search)) {
            $data = $request->search;
            $query = $query->where(function($q) use ($data){
                $q->whereRaw('LOWER(question_text) like ?', ['%' . strtolower($data) . '%']);
            });
        }

        return $query->paginate($request->itemsPerPage);
    }

    public function create($request, $exercise_id)
    {
        //last question exist
        $last = Question::where('exercise_id', $exercise_id)->orderBy('seq', 'desc')->first();

        //if exist
        if($last){
            $seq = $last->seq + 1;
        }
        else{
            $seq = 1;
        }

        $question = new Question();
        $question->exercise_id = $exercise_id;
        $question->seq = $seq;
        $question->topic_id = $request->question['topic_id'];
        $question->question_text = $request->question['question_text'];
        $question->question_type = $request->question['question_type'];
        $question->answer_explanation = $request->question['answer_explanation'];
        $question->save();

        if($question->question_type == 1){

            $question_option = $request->question_option;

            if($question_option){
                foreach($question_option as $option){
                    $opt = new QuestionsOption();
                    $opt->question_id = $question->id;
                    $opt->option = $option['option'];

                    if($option['correct']){
                        $opt->correct = 1;
                    }
                    else{
                        $opt->correct = 0;
                    }
                    
                    $opt->save();
                }
            }
        }
        else if($question->question_type == 2){
            $sample_answer = $request->sample_answer;

            if($sample_answer){
                foreach($sample_answer as $sample_ans){
                    $sa = new SampleAnswer();
                    $sa->question_id = $question->id;
                    $sa->text = $sample_ans['text'];
                    $sa->save();
                }
            }
        }

        return $question;
    }

    public function edit($request, $question_id)
    {
        $question = Question::find($question_id);

        $question->update([
            'topic_id' => $request->question['topic_id'],
            'question_text' => $request->question['question_text'],
            'answer_explanation' => $request->question['answer_explanation']
        ]);

        if($question->question_type == 1){ //soalan objective
            $question_option = QuestionsOption::where('question_id', $question_id)->get();

            $req_count = count($request->question_option);
            // dd($req_count);
            $row_count = count($question_option);
            // dd($row_count);

            if($question_option){
                $index = 0;

                foreach($question_option as $row){ //replace the request data to the existing row. by looping the row
                    if($index < $req_count){
                        $row->update([
                            'option' => $request->question_option[$index]['option'],
                            'correct' => $request->question_option[$index]['correct'],
                        ]);
                    }
                    else if($index >= $req_count){ //when the req data < row, then delete the remaining row
                        $row->delete();
                    }

                    $index = $index + 1;
                }

                for($i = $index; $i < $req_count; $i++){ //if req data > $row. add the data, add more row
                    $opt = new QuestionsOption();
                    $opt->question_id = $question->id;
                    $opt->option = $request->question_option[$i]['option'];

                    if($request->question_option[$i]['correct']){
                        $opt->correct = 1;
                    }
                    else{
                        $opt->correct = 0;
                    }
                    
                    $opt->save();
                }
            }
        }
        else if($question->question_type == 2){ // soalan subjective
            $sample_answer = SampleAnswer::where('question_id', $question_id)->get();

            $req_count = count($request->sample_answer);
            // dd($req_count);
            $row_count = count($sample_answer);
            // dd($row_count);

            if($sample_answer){
                $index = 0;

                foreach($sample_answer as $row){ //replace the request data to the existing row. by looping the row
                    if($index < $req_count){
                        $row->update([
                            'text' => $request->sample_answer[$index]['text']
                        ]);
                    }
                    else if($index >= $req_count){ //when the req data < row, then delete the remaining row
                        $row->delete();
                    }

                    $index = $index + 1;
                }

                for($i = $index; $i < $req_count; $i++){ //if req data > $row. add the data, add more row
                    $sa = new SampleAnswer();
                    $sa->question_id = $question->id;
                    $sa->text = $request->sample_answer[$i]['text'];
                    
                    $sa->save();
                }
            }
        }

        return $question;
    }

    public function getExerciseQuestions($exercise_id)
    {
        $questions = Question::select('question_text','id','seq')
                    ->where('exercise_id', $exercise_id)
                    ->orderBy('seq', 'asc')
                    ->get();

        //loop thru all the question to get its options
        foreach($questions as &$question){
            $question->options = QuestionsOption::select('id','option','image')
                                ->where('question_id', $question->id)
                                ->get();

            foreach($question->options as $opt){
                if($opt->image)
                    $opt->image_url = env('APP_URL') . $opt->image;
            }
        }

        // $questions = Exercise::find($exercise_id)->questions()->get();

        // foreach($questions as &$question){
        //     $question->options = QuestionsOption::where('question_id', $question->id)->get();
        // }

        return $questions;
    }
}