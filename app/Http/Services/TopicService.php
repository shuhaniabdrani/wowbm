<?php

namespace App\Http\Services;
use App\Models\Topic;

class TopicService{

    public function getTopicPaginate($request)
    {
        // nak buat topic::all() cannot, so alterntive hm
        $query = Topic::whereNotNull('topic_name');

        if (isset($request->search)) {
            $data = $request->search;
            $query = $query->where(function($q) use ($data){
                $q->whereRaw('LOWER(topic_name) like ?', ['%' . strtolower($data) . '%'])
                ->orWhereRaw('LOWER(description) like ?', ['%' . strtolower($data) . '%']);
            });
        }

        return $query->paginate($request->itemsPerPage);
    }

    public function create($request)
    {
        $topic = new Topic;
        $topic->topic_name = $request->topic_name;
        if(isset($topic->description))
            $topic->description = $request->description;
        $topic->save();

        return $topic;
    }

    public function edit($request)
    {
        $topic = Topic::find($request->id);

        $topic->update([
            'topic_name' => $request->topic_name,
            'description' => $request->description
        ]);

        return $topic;
    }
}