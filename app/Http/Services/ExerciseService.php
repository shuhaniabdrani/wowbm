<?php

namespace App\Http\Services;
use Illuminate\Support\Facades\Auth;
use App\Models\Exercise;
use App\Models\Question;
use App\Models\QuestionsOption;
use App\Models\Answer;
use App\Models\Attempt;
use App\User;
use Symfony\Component\Console\Question\Question as QuestionQuestion;

class ExerciseService{

    public function getExercisePaginate($request)
    {
        if(isset($request->type)){
            if($request->type == 'reading'){
                $query = Exercise::where('exercise_type_id', 1);
            }
            else if($request->type == 'writing'){
                $query = Exercise::where('exercise_type_id', 2);
            }
            else if($request->type == 'listening'){
                $query = Exercise::where('exercise_type_id', 3);
            }
            else if($request->type == 'speaking'){
                $query = Exercise::where('exercise_type_id', 4);
            }
        }
        else{
            $query = Exercise::whereIn('exercise_type_id', array(1, 2, 3, 4));
        }
        

        if (isset($request->search)) {
            $data = $request->search;
            $query = $query->where(function($q) use ($data){
                $q->whereRaw('LOWER(exercise_name) like ?', ['%' . strtolower($data) . '%']);
            });
        }

        return $query->paginate($request->itemsPerPage);
    }

    public function create($request)
    {
        $exercise = new Exercise();
        $exercise->exercise_name = $request->exercise_name;
        $exercise->exercise_type_id = $request->exercise_type_id;
        $exercise->save();

        // $questions = $request->questions;

        // if($questions){
        //     foreach($questions as $question){
        //         $que = Question::find($question['question_id']);
        //         $que->exercises()->syncWithoutDetaching($exercise->id);
        //         // https://stackoverflow.com/questions/49782303/laravel-5-6-many-to-many-relationship-sync
        //     }
        // }
    }

    public function edit($request)
    {
        $exercise = Exercise::find($request->id);
        $exercise->exercise_type_id = $request->exercise_type_id;
        $exercise->exercise_name = $request->exercise_name;
        $exercise->save();

        return $exercise;
    }

    public function getExerciseData($exercise_id)
    {
        $exercise = Exercise::find($exercise_id);

        return $exercise;

    }

    public function getAllExercise()
    {
        $list = Exercise::all();

        return $list;
    }

    public function saveAnswer($request, $exercise_id)
    {
        $attempt = new Attempt();
        $attempt->user_id = Auth::user()->id;
        $attempt->exercise_id = $exercise_id;
        $attempt->total_questions = count($request->data);
        $attempt->result = 0;
        $attempt->save();
        
        $result = 0;

        foreach($request->data as $data){

            $topic_data = Question::select('topic_id')->find($data['question_id']);
            $answer = new Answer();
            $answer->attempt_id = $attempt->id;
            $answer->question_id = $data['question_id'];

            if(isset($data['option_id'])){
                $option_data = QuestionsOption::select('question_id', 'correct')->find($data['option_id']);
                $answer->option_id = $data['option_id'];
                $answer->correct = $option_data->correct;
            }
            else{
                $answer->option_id = 0;
                $answer->correct = 0;
            }
            
            $answer->topic_id = $topic_data->topic_id;
            $answer->save();

            if($answer->correct == 1){
                $result = $result + 1;
            }
        }

        $attempt->update(['result' => $result]);

        return $attempt;
    }

    public function getAttemptResult($attempt_id)
    {
        // $answer = Attempt::find($attempt_id)->answers()->get();

        // foreach($answer as &$ans){
        //     $ans->question = Question::find($ans->question_id);

        //     $ans->question->options = QuestionsOption::where('question_id', $ans->question_id)->get();
        // }

        // return $answer;

        $attempt = Attempt::find($attempt_id);

        $attempt->user = User::find($attempt->user_id);

        $attempt->answer = Attempt::find($attempt_id)->answers()->get();

        // $attempt->answer = Answer::select('option_id', 'correct')->where('attempt_id', $attempt_id)->get();

        foreach($attempt->answer as &$ans){
            $ans->question = Question::select('question_text', 'answer_explanation')->find($ans->question_id);

            $ans->question->options = QuestionsOption::select('id','option', 'image', 'correct')->where('question_id', $ans->question_id)->get();

            foreach($ans->question->options as $opt){
                if($opt->image)
                    $opt->image_url = env('APP_URL') . $opt->image;
            }
        }


        return $attempt;
    }

    public function getExerciseResultPaginate($request)
    {
        $query = Exercise::Join('attempts', 'exercises.id', '=', 'attempts.exercise_id')->where('attempts.user_id', Auth::user()->id)->orderBy('attempts.updated_at','desc');

        if (isset($request->search)) {
            $data = $request->search;
            $query = $query->where(function($q) use ($data){
                $q->whereRaw('LOWER(exercises.exercise_name) like ?', ['%' . strtolower($data) . '%']);
            });
        }

        return $query->paginate($request->itemsPerPage);
    }


}