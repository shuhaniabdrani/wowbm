<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $password = $this->data['password'];
        $email = $this->data['email'];
        return $this->from('system@wowbm.com', 'WOWBM')
            ->subject('WOWBM system')
            ->view('mail.welcome-email',  $beautymail->getData())
            ->with('password', $password)
            ->with('email', $email);
    }
}
