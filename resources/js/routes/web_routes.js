import VueRouter from 'vue-router'
import { setPageTitle } from '@/js/helpers/common'

// Layout
import BaseLayout from '@/js/layouts/BaseLayout'

// Pages
import Login from '@/js/views/General/Login'
import Register from '@/js/views/General/Register'
import ForgotPassword from '@/js/views/General/Forgotpassword'
import ResetPassword from '@/js/views/General/Resetpassword'
import Main from '@/js/views/General/Main'
import Error404 from '@/js/views/General/404'
import Comingsoon from '@/js/views/General/Comingsoon'

import Home from '@/js/views/Home/User'
import HomeAdmin from '@/js/views/Home/Admin'


//admin
import TopicList from '@/js/views/Topic'

import QuestionList from '@/js/views/Question'
import QuestionCreate from '@/js/views/Question/create'
import QuestionEdit from '@/js/views/Question/edit'

import ExerciseList from '@/js/views/Exercise'

//user
import ExerciseType from '@/js/views/Exercise/exerciseType'
import ListExercise from '@/js/views/Exercise/exerciseList'
import AttemptExercise from '@/js/views/Exercise/attempt'
import Result from '@/js/views/Exercise/result'

import ListTopic from '@/js/views/Topic/topicList'
import AnalysisType from '@/js/views/Analysis/analysisType'
import ResultList from '@/js/views/Analysis/resultList'
import topicAnalysis from '@/js/views/Analysis/topicAnalysis'

import Vocabulary from '@/js/views/Vocabulary/index'
import ViewNote from '@/js/views/Topic/view'


// Routes
// ** for more information, kindy refer to https://github.com/websanova/vue-auth/blob/master/docs/Privileges.md
// ** auth: true <- can be access only if auth.check = true
// ** auth: false <- cannot be access is auth.check = true
// ** auth: null, title: '' <- no auth rule
// ** auth: [1,2] <- can be access is auth.user.role matched

const baseLayoutRoutes = [
    { path: '/comingsoon', name: 'admin.home2', component: Comingsoon, meta: { auth: [ 1,5 ], title: 'Coming Soon' } },
    
    //home
    { path: '/admin', name: 'home', component: HomeAdmin, meta: { auth: { roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Admin Panel' } },
    { path: '/home', name: 'adminhome', component: Home, meta: { auth: { roles: [ 2,3 ], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Home' } },
    
    //***admin***
    //topic
    { path: '/topic', name: 'topic', component: TopicList, meta: { auth: { roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Topic List' } },

    // exercise
    { path: '/exercise', name: 'exercise', component: ExerciseList, meta: { auth: { roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Exercise List' } },
    { path: '/exercise/:exercise_name', name: 'exercise.questionlist', component: QuestionList, meta: { auth: { roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Question List' } },
    { path: '/exercise/:exercise_name/question/create', name: 'question.create', component: QuestionCreate, meta: { auth: { roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Create Question' } },
    { path: '/exercise/:exercise_name/question/:question_id/edit', name: 'question.edit', component: QuestionEdit, meta: { auth: { roles: 1, redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Edit Question' } },
    
    //***users***
    { path: '/exercisetype', name: 'exercise.type', component: ExerciseType, meta: { auth: { roles: [ 2,3 ], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Exercise Type' } },
    { path: '/exercisetype/:exercise_type', name: 'exercise.list', component: ListExercise, meta: { auth: { roles: [ 2,3 ], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Exercise List' } },
    { path: '/exercise/:exercise_name/attempt', name: 'exercise.attempt', component: AttemptExercise, meta: { auth: { roles: [ 2,3 ], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Attempt Exercise' } },
    { path: '/result/:attempt_id', name: 'exercise.result', component: Result, meta: { auth: { roles: [ 2,3 ], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Result' } },

    { path: '/notes', name: 'topic.list', component: ListTopic, meta: { auth: { roles: [ 2,3 ], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Topic List' } },

    { path: '/analysistype', name: 'analysis.type', component: AnalysisType, meta: { auth: { roles: [2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Analysis Type' } },
    { path: '/analysistype/result/list', name: 'result.list', component: ResultList, meta: { auth: { roles: [2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Result List' } },
    { path: '/analysistype/topic/list', name: 'topic.analysis', component: topicAnalysis, meta: { auth: { roles: [2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Topic Analysis' } },

    { path: '/vocabulary', name: 'vocabulary', component: Vocabulary, meta: { auth: { roles: [1,2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'Vocabulary' } },
    { path: '/topic/:topic_id/view', name: 'topic.view', component: ViewNote, meta: { auth: { roles: [1,2,3], redirect: {name: 'login'}, forbiddenRedirect: '/403'}, title: 'View Note' } },
]

const routes = [

    
    { path: '/', name: 'main', component: Login, meta: { auth: null, title: 'Home' } },
    { path: '/login', name: 'login', component: Login, meta: { auth: null, title: 'Login' } },
    { path: '/register', name: 'register', component: Register, meta: { auth: null, title: 'Register' } },
    { path: '/forgot-password', name: 'forgotpassword', component: ForgotPassword, meta: { auth: null, title: 'Forgot Password' } },
    { path: '/reset/password/:token', name: 'resetpassword', component: ResetPassword, meta: { auth: null, title: 'Reset Password' } },
    // { path: '/403', name: '403', component: Error403, meta: { auth: null, title: '403' } },

    
   
    // base_layout
    { path: '/', name: 'base_layout', component: BaseLayout, children: baseLayoutRoutes },  
    { path: '*', name: '404', component: Error404, meta: { auth: null, title: '404' } },
]

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
})

// if url doesnot match any register routes
router.beforeEach((to, from, next) => {
    setPageTitle(to.meta.title)
    next()
    // if (!to.matched.length) {
    //     next('/404');
    // }else {
    //     next()
    // }
});

export default router