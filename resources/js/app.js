import './bootstrap'
import config from '@/js/configs'

import Vue from 'vue'
import router from './routes/web_routes'
import VueRouter from 'vue-router'
import '@mdi/font/css/materialdesignicons.min.css';
import 'vuetify/dist/vuetify.min.css';
import Vuetify from 'vuetify';
import VuetifyToast from 'vuetify-toast-snackbar'
import axios from 'axios'
import VueAxios from 'vue-axios'
import auth from './auth'
import VueAuth from '@websanova/vue-auth'
import api from './api/api_routes'
import { errorHandler } from "@/js/helpers/common"
import * as filters from '@/js/filters'; // global filters
import Index from './views/index'
import Loading from 'vue-loading-overlay';
import Vuelidate from 'vuelidate';
import VueCookies from 'vue-cookies'
import Vue2Editor from "vue2-editor";

// Set Vue globally

window.Vue = Vue

// Set Vue router

Vue.router = router
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
};
Vue.use(VueRouter)


Vue.use(VueCookies)
$cookies.config('7d') // cookies exp

// vuetify

Vue.use(Vuetify);

// Vuelidate
Vue.use(Vuelidate)

// vuetify snackbar
Vue.use(VuetifyToast, {
    x: 'right', // default
    y: 'bottom', // default
    color: 'info', // default
    icon: 'info',
    iconColor: '', // default
    classes: [
        'body-2'
    ],
    timeout: 3000, // default
    dismissable: true, // default
    multiLine: false, // default
    vertical: false, // default
    queueable: false, // default
    showClose: false, // default
    closeText: '', // default
    closeIcon: 'mdi-close', // default
    closeColor: '', // default
    slot: [], //default
    shorts: {
        custom: {
            color: 'purple'
        }
    },
    property: '$toast' // default
})

const vuetifyOptions = {
    theme: {
        dark : false,
        themes: {
            light: {
                primary: config.theme.primary,
                secondary: config.theme.secondary,
                background: config.theme.background
            },
            dark: {
                primary: config.theme.secondary,
                secondary: config.theme.primary,
                background: config.theme.background
            },
        },
    }
}

// vue-loading-overlay
Vue.use(Loading,{
    loader: 'Bars',
    color: '#57BD7A',
    height: 64,
    width: 64,
  });




// Set Vue authentication

Vue.use(VueAxios, axios)
var baseurl = window.location.protocol + "//" + window.location.host;
axios.defaults.baseURL = baseurl+`/api`


Vue.use(VueAuth, auth)

// Filter :: register global utility filters.

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key]);
});



// Prototype

Vue.prototype.$api = api
Vue.prototype.$errorHandler = errorHandler

Vue.mixin({
    methods: {
      messageErrors: function(key, label) {
        const errors = []
        if(key.$params.required && key.$error && !key.required)
        {
          errors.push(label + ' is required')
          return errors
        }
        else if(key.$params.email && key.$error && !key.email)
        {
          errors.push(label + ' invalid')
          return errors
        }
        else if(key.$params.numeric && key.$error && !key.numeric)
        {
          errors.push(label + ' must be a number')
          return errors
        }
        else if(key.$params.maxValue && key.$error && !key.maxValue){
          errors.push(label + ' max value is exceed')
          return errors
        }
        else if(key.$params.minValue && key.$error && !key.minValue){
          errors.push(label + ' min value is exceed')
          return errors
        }
        // else if(key.$params && key.$error && !key.sameAs){
        //   errors.push(label + ' not match')
        //   return errors
        // }
          return errors
      },
    }
  })

Vue.use(Vue2Editor);

// Load Index

Vue.component('index', Index)

// initialize vue app
const app = new Vue({
    el: '#app',
    router,
    vuetify: new Vuetify(vuetifyOptions),
});
