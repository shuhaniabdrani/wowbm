export default {

	navbar_path: {
		'admin' : [
			{
				title : "General",
				routes : [
					{ 
						icon: "mdi-home", title : "Home", route: "/admin", module : "adminhome", sub: null 
					},
				]
			},
			{
				title : "Content Management",
				routes : [
					{ 
						icon: "mdi-book", title : "Topic", route: "/topic", module : "topic", sub: null 
					},
					{ 
						icon: "mdi-note", title : "Exercise", route: "/exercise", module : "exercise", sub: null 
					},
					{ 
						icon: "mdi-translate", title : "Vocabulary", route: "/vocabulary", module : "vocabulary", sub: null 
					},
				]
			},
		],
		'student' : [
			{
				title : "General",
				routes : [
					{ 
						icon: "mdi-home", title : "Home", route: "/home", module : "home", sub: null 
					},
				]
			},
			{
				title : "Activity",
				routes : [
					{ 
						icon: "mdi-book", title : "Notes", route: "/notes", module : "topic", sub: null 
					},
					{ 
						icon: "mdi-file", title : "Exercises", route: "/exercisetype", module : "exercise.type", sub: null 
					},
					{ 
						icon: "mdi-translate", title : "Vocabulary", route: "/vocabulary", module : "vocabulary", sub: null 
					},
					{ 
						icon: "mdi-chart-box", title : "Analysis", route: "/analysistype", module : "analysis", sub: null 
					},
					
				]
			},
		]
	}

};
