<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>{{ $title }}</title>
 <style>
  table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    table-layout: fixed;
    width: 100px;
  }
  th{
    padding: 5px;
    text-align: center;  
    background-color: #D3D3D3;  
  }
  img{
    max-width: 476.5px;
	  max-height: 198px;
  }
  h3{
    text-align: center; 
  }
  .subhead{
    text-align: center; 
  }
  .center{
    text-align: center;
  }
  </style>
</head>
<body>
  <p class='center'>
    <img src="{{url('/assets/aalogo.png')}}"></img>
  <p>
  <h3>{{$heading}}</h3>
  <div>
    Post Applied: <u>{{$data->position->post}}</u> <br/>
     How Do You Know The Job Vacancy? Please State. <u>{{$data->position->howKnow}}</u>
  </div>
  <br/>
  <div>
  <table style="width:100%">
    <tr>
      <th colspan="12">Personal Particulars</th>
    </tr>
    <tr>
      <td colspan="12">Name : {{$data->user->name}}</td>
    </tr>
    <tr>
      <td colspan="12">Address : {{$data->personal->address}}</td>
    </tr>
    <tr>
      <td colspan="3">Telephone. No</td>
      <td colspan="3">House : {{$data->personal->houseNo}}</td>
      <td colspan="3">Office : {{$data->personal->officeNo}}</td>
      <td colspan="3">Handphone : {{$data->personal->hpNo}}</td>
    </tr>
    <tr>
      <td colspan="6">Identity Card No. / Color : {{$data->personal->ic}}</td>
      <td colspan="6">Passport No. : {{$data->personal->passport}}</td>
    </tr>
    <tr>
      <td colspan="6">EPF No. : {{$data->personal->epf}}</td>
      <td colspan="6">SOCSO No. : {{$data->personal->socso}}</td>
    </tr>
    <tr>
      <td colspan="12">Email Address : {{$data->user->email}}</td>
    </tr>
    <tr>
      <td colspan="3">Age: {{$data->personal->age}}</td>
      <td colspan="3">Sex : {{$data->personal->sex}}</td>
      <td colspan="6">Marital Status : {{$data->personal->status}}</td>
    </tr>
    <tr>
      <td colspan="4">Religion: {{$data->personal->religion}}</td>
      <td colspan="4">Race : {{$data->personal->race}}</td>
      <td colspan="4">Nationality : {{$data->personal->nationality}}</td>
    </tr>
  </table>
  <br/>
  <table style="width:100%">
    <tr>
      <th colspan="12">Family Particulars</th>
    </tr>
    <tr>
      <td colspan="6">Name of Wife / Husband : {{$data->family->wh_name}}</td>
      <td colspan="6">Occupation : {{$data->family->wh_occupation}}</td>
    </tr>
    <tr>
      <td colspan="6">Employer's name : {{$data->family->wh_empName}}</td>
      <td colspan="6">Telephone. No : {{$data->family->wh_empNo}}</td>
    </tr>
    <tr>
      <th colspan="12">No. of Children / Dependant</th>
    </tr>
    <tr>
      <td class="subhead" colspan="1">No</td>
      <td class="subhead" colspan="4">Full Name</td>
      <td class="subhead" colspan="2">Date of Birth </td>
      <td class="subhead" colspan="2">Relationship</td>
      <td class="subhead" colspan="3">School</td>
    </tr>

    @foreach($data->family->dependant as $dep)
    <tr>
      <td colspan="1">{{$loop->index + 1}}</td>
      <td colspan="4">{{$dep->name}}</td>
      <td colspan="2">{{date('d-m-Y', strtotime($dep->birthDate))}} </td>
      <td colspan="2">{{$dep->relationship}}</td>
      <td colspan="3">{{$dep->school}}</td>
    </tr>
    @endforeach

    <tr>
      <td colspan="12">Name of Father : {{$data->family->father_name}}</td>
    </tr>
    <tr>
      <td colspan="6" rowspan="2">Permanent Address : {{$data->family->father_address}}</td>
      <td colspan="6">Occupation : {{$data->family->father_occupation}}</td>
    </tr>
    <tr>
      <td colspan="6">Telephone. No : {{$data->family->father_no}}</td>
    </tr>
    <tr>
      <td colspan="12">Name of Mother : {{$data->family->mother_name}}</td>
    </tr>
    <tr>
      <td colspan="6" rowspan="2">Permanent Address : {{$data->family->mother_address}}</td>
      <td colspan="6">Occupation : {{$data->family->mother_occupation}}</td>
    </tr>
    <tr>
      <td colspan="6">Telephone. No : {{$data->family->mother_no}}</td>
    </tr>
  </table>
  <br/>

  <table style="width:100%">
    <tr>
      <th colspan="12">Driving Qualification</th>
    </tr>
    <tr>
      <td colspan="4">Driving License No. : {{$data->driving->houseNo}}</td>
      <td colspan="4">Class : {{$data->driving->class}}</td>
      <td colspan="4">Driving Experience(Years) : {{$data->driving->drivingExp}}</td>
    </tr>
    <tr>
      <td colspan="12">Do you have any transport? {{$data->driving->haveTransport}}</td>
    </tr>
  </table>
  <br/>

  <table style="width:100%">
    <tr>
      <th colspan="12">Academic Qualification</th>
    </tr>
    <tr>
      <td class="subhead" colspan="4">School / College / University</td>
      <td class="subhead" colspan="2">Place</td>
      <td class="subhead" colspan="2">Period </td>
      <td class="subhead" colspan="4">Certificate / Diploma / Degree</td>
    </tr>

    @foreach($data->academic as $academic)
    <tr>\
      <td colspan="4">{{$academic->school}}</td>
      <td colspan="2">{{$academic->place}} </td>
      <td colspan="2">{{$academic->period}}</td>
      <td colspan="4">{{$academic->cert}}</td>
    </tr>
    @endforeach
  </table>
  <br/>

  <table style="width:100%">
    <tr>
      <th colspan="12">Language</th>
    </tr>
    <tr>
      <td class="subhead" colspan="4">Language / Dialect</td>
      <td class="subhead" colspan="4">Spoken (Fair / Good / Excellent)</td>
      <td class="subhead" colspan="4">Written (Fair / Good / Excellent)</td>
    </tr>

    @foreach($data->language as $language)
    <tr>\
      <td colspan="4">{{$language->type}}</td>
      <td colspan="4">{{$language->spoken}} </td>
      <td colspan="4">{{$language->written}}</td>
    </tr>
    @endforeach
  </table>
  <br/>

  <table style="width:100%">
    <tr>
      <th colspan="12">Programming Language</th>
    </tr>
    <tr>
      <td class="subhead" colspan="4">Programming Language</td>
      <td class="subhead" colspan="2">Rates (1-10)</td>
      <td class="subhead" colspan="2">Years of experience</td>
      <td class="subhead" colspan="4">Programming Framework</td>
    </tr>

    @foreach($data->programming as $programming)
    <tr>\
      <td colspan="4">{{$programming->type}}</td>
      <td colspan="2">{{$programming->rate}} </td>
      <td colspan="2">{{$programming->year}}</td>
      <td colspan="4">{{$programming->framework}}</td>
    </tr>
    @endforeach
  </table>
  <br/>

  <table style="width:100%">
    <tr>
      <th colspan="12">Working Experience</th>
    </tr>
    <tr>
      <td class="subhead" colspan="2">Position</td>
      <td class="subhead" colspan="4">Employer & Address </td>
      <td class="subhead" colspan="2">Period<br/>From : To</td>
      <td class="subhead" colspan="2">Salary</td>
      <td class="subhead" colspan="2">Reason For Leaving</td>
    </tr>

    @foreach($data->working->experience as $experience)
    <tr>\
      <td colspan="2">{{$experience->position}}</td>
      <td colspan="4">{{$experience->employer}} </td>
      <td colspan="2">{{$experience->period}}</td>
      <td colspan="2">{{$experience->salary}}</td>
      <td colspan="2">{{$experience->reasonleave}}</td>
    </tr>
    @endforeach

    <tr>
      <td colspan="12">Do you have any objections if our Company contacts your previous employer? {{$data->working->contactOldEmp}}</td>
    </tr>
  </table>
  <br/>

  <table style="width:100%">
    <tr>
      <th colspan="12">Declaration</th>
    </tr>
    <tr>
      <td class="subhead" colspan="12">Name of 2 referees (not including family members)</td>
    </tr>
    <tr>
      <td colspan="6">Full Name : {{$data->declaration->first_name}}</td>
      <td colspan="6">Full Name : {{$data->declaration->second_name}}</td>
    </tr>
    <tr>
      <td colspan="6">Address : {{$data->declaration->first_address}}</td>
      <td colspan="6">Address : {{$data->declaration->second_address}}</td>
    </tr>
    <tr>
      <td colspan="3">Occupation : {{$data->declaration->first_occupation}}</td>
      <td colspan="3">Telephone. No : {{$data->declaration->first_No}}</td>
      <td colspan="3">Occupation : {{$data->declaration->second_occupation}}</td>
      <td colspan="3">Telephone. No : {{$data->declaration->second_No}}</td>
    </tr>
    <tr>
      <td colspan="12">Have you ever been charged or convicted in any Court? {{$data->declaration->charge}}</td>
    </tr>
    <tr>
      <td colspan="12">Have you been or are you suffering from any physical impairment or disease? {{$data->declaration->disease}} <br/>
      Please state illiness and period of time : {{$data->declaration->ill_time}}
      </td>
    </tr>
    <tr>
      <td colspan="7">If selected, when will you be able to commence employment : {{date('d-m-Y', strtotime($data->declaration->startDate))}}</td>
      <td colspan="5">Expected salary : {{$data->declaration->expSalary}}</td>
    </tr>
  </table>
  <br/>

  <table style="width:100%">
    <tr>
      <th colspan="12">In case of emergency, please contact :</th>
    </tr>
    <tr>
      <td colspan="6">Name : {{$data->emergency->name}}</td>
      <td colspan="6">Telephone No. : {{$data->emergency->telNo}}</td>
    </tr>
    <tr>
      <td colspan="6">Relationship : {{$data->emergency->relationship}}</td>
      <td colspan="6">Address : {{$data->emergency->address}}</td>
    </tr>
  </table>
  <br/>
  
  <table style="width:100%">
    <td colspan="6">Signature:<br/><img src="{{$data->signature->sign}}" id="signature"></img></td>
    <td colspan="6">Date : {{ date('d-m-Y', strtotime($data->signature->date_submit))}}</td>
  </table>
  </div>
</body>
</body>
</html>