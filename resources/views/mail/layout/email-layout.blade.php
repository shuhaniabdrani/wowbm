<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>TESTINGGG</title>
</head>
<html>
    <body style="background-color: #EFEFEF;">
        <center>
            <table style="padding: 3rem !important; max-width: 600px; margin: 0 auto; display: block; background: #FFFFFF; margin-top:50px; border-radius: 0.25rem !important;">
                <tr>
                    <!-- HEADER -->
                    <td align="middle" style="width: 100%;">
                        <img src="{{ asset('img/logo-in.png') }}" style="width: 100px;" alt=""><br> 
                        <span style="font-size: 1.3rem; color: #191919; font-weight: 900; line-height: 80%; ">Advisory Apps Sdn Bhd</span>
                    <!-- /content -->
                    </td>
                </tr>
                <!-- /HEADER -->
                <!-- BODY -->
                <tr>
                    @yield('content')
                </tr>
                <!-- /BODY -->
            </table>
        </center>
    </body>
</html>